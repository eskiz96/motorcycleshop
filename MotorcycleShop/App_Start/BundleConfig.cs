﻿using System.Web;
using System.Web.Optimization;

namespace MotorcycleShop
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/jquery-1.11.3.min.js",
                      "~/Scripts/bootstrap.min.js",
                      "~/Scripts/modernizr.custom.js",
                      "~/assets/rendro-easy-pie-chart/dist/jquery.easypiechart.min.js",
                      "~/Scripts/waypoints.min.js",
                      "~/Scripts/jquery.easypiechart.min.js",
                      "~/Scripts/classie.js",
                      "~/assets/switcher/js/switcher.js",
                      "~/assets/owl-carousel/owl.carousel.min.js",
                      "~/assets/bxslider/jquery.bxslider.min.js",
                      "~/assets/slider/jquery.ui-slider.js",
                      "~/assets/isotope/isotope.js",
                      "~/assets/slider/jquery.ui-slider.js",
                      "~/assets/fancybox/jquery.fancybox.pack.js",
                      "~/Scripts/jquery.smooth-scroll.js",
                      "~/Scripts/wow.min.js",
                      "~/Scripts/jquery.placeholder.min.js",
                      "~/Scripts/theme.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/master.css",
                      "~/assets/switcher/css/switcher.css",
                      "~/assets/switcher/css/color5.css"));
        }
    }
}


    